    var createObserver = function (myName, action, label, options,) {

        return new IntersectionObserver(entries => { 
            entries.forEach(entry => {
                const [{isIntersecting}] = entries;
                if(isIntersecting && entry.time > 2000) {
                    console.log( {
                        'event-name': myName,
                        'event-action': action,
                        'event-label': label,
                        'time-stamp': Date.now()   
                    })
                    //uncomment to send to data layer
                    // triggerAction(myName,action,label)
                }
            })
        },options)
    }  
    
    var triggerAction = function (myName, action, label, link = null) {
        window.dataLayer = window.dataLayer || [];
        dataLayer.push({
          event: 'data-layer-event',
          eventDetails: {
            eventInfo: {
              eventName: myName,
              eventAction: action,
              eventLabel: label,
              timestamp: Date.now(),
            },
            attributes: {
              item: {
                itemOutboundLink: link,
              },
            },
          },
        });
      
        if (link) {
          var target = link.startsWith('tel:') ? '_self' : '_blank';
          setTimeout(function () {
            window.open(link, target);
          }, 250);
        }
      };
      
      var setupListeners = function setupListeners() {

        ////////////////////// OBSERVER SET UP  
   
        //Grab targets
        const topTenHoldings = document.querySelector('#top-10-holdings');
        const growthOfTenK = document.querySelector('#growth-of-10k');

        //Set Options
        const options = {
            root: null,                
            threshold: [0.3, 0.7],
            delay: 2000     
        }

        // Create Observers
        const topTenObserver = createObserver('ncaa | QQQ | content-interaction','cta Dwell','top 10 holdings',options);
        const growthObserver = createObserver('ncaa | QQQ | content-interaction','cta Dwell','growth of 10k',options)


        window.addEventListener('scroll',() => {
            if (window.scrollY > 0) {
                topTenObserver.observe(topTenHoldings);
                growthObserver.observe(growthOfTenK);
                }
            })
        }

      if (document.readyState != 'complete') {
        window.addEventListener('load', setupListeners);
      } else {
        setupListeners();
      }








 

